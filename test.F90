#define assert(E) if(.not.(E))then;write(stderr,'(a,":",i0,1x,"assert(E) failed")')__FILE__,__LINE__;call exit_p(-1);endif

    subroutine s_test()
#ifdef USE_MPI
    use mpi
#endif
    use h5
    use iso_c_binding, only: c_null_char, c_ptr, c_loc
    use iso_fortran_env, only : p => real64
    implicit none


    type(h5file) :: f
    type(h5dataset) :: d

    integer :: i
    real(p), target :: v(4,5)
    complex(p) :: vc(3,4)

    integer :: ip, np, comm, err
    real(p) :: v_ref(4,5), diff
    type(h5space) :: s

    character(len=60), target :: sv(4)



#ifdef USE_MPI
    comm = mpi_comm_world

    call mpi_comm_size(comm, np, err)
    call mpi_comm_rank(comm, ip, err)
#else
    comm = -1
    np = 1
    ip = 0
#endif

! serial stuff
    if (ip == 0) then
        call h5_write('sclf.h5:/scalar', 6.28_8, flags=h5f_acc_trunc)
        call h5_read('sclf.h5:/scalar', v(1,1))

        diff = v(1,1) - 6.28_8
        print *, 'scl diff', diff
        assert(diff < 1e-12_p)


        call h5_write('sclf.h5:/scalar', 6.28_8, dtype=h5t_native_double, flags=h5f_acc_trunc)
        call h5_read('sclf.h5:/scalar', v(1,1), dtype=h5t_native_double)

        diff = v(1,1) - 6.28_8
        print *, 'scl diff', diff
        assert(diff < 1e-12_p)

        call h5_write('sclf.h5:/scalarv', v(1,1), dtype=find_h5dtype(v))
        call h5_read('sclf.h5:/scalarv', v(2,1), dtype=find_h5dtype(v))

        diff = v(2,1) - v(1,1)
        print *, 'scl diff2', diff
        assert(diff < 1e-12_p)

        call h5_write('sclf.h5:/scalarva', v(1:1,1), dtype=find_h5dtype(v))
        call h5_read('sclf.h5:/scalarva', v(2,1), dtype=find_h5dtype(v))

        diff = v(2,1) - v(1,1)
        print *, 'scl diff2a', diff
        assert(diff < 1e-12_p)



        v = reshape([(real(i+0.5_p,kind=p),i=1,size(v))],shape(v))

        vc = reshape([(cmplx(i*0.9_p,i*0.3_p,kind=p),i=1,size(vc))],shape(vc))


        call h5_write('f.h5:/some/path/vals', v, dtype=find_h5dtype(v), flags=h5f_acc_trunc)

        call f % init('f.h5')
        call f % write('/some/other/stuff', v*0.1_p, dtype=find_h5dtype(v))
        call f % close()


        call f % init('f.h5')
        call d % init(f, '/ds1', dtype=find_h5dtype(v), fdims=[4,5]*1_8, mdims=[4,5]*1_8)
        ! call d % init(f, '/ds1', v, fdims=[4,5]*1_8, mdims=[4,5]*1_8)
        call d % write(v, fidx=[0,2]*1_8, fcnt=[4,1]*1_8, midx=[0,2]*1_8, mcnt=[4,1]*1_8)
        call d % write(v, fidx=[3,0]*1_8, fcnt=[1,5]*1_8, midx=[3,0]*1_8, mcnt=[1,5]*1_8)
        call d % close()
        call f % close()

        call h5_write('f.h5:/some/path/vcp', vc, dtype=find_h5dtype(vc), &
            fdims=[3,4]*1_8, fidx=[0,2]*1_8, fcnt=[3,1]*1_8, mdims=[3,4]*1_8, midx=[0,2]*1_8, mcnt=[3,1]*1_8)

        block
            type(c_ptr) :: cp(4)

            sv = ["yhart"//c_null_char, "ffsfr"//c_null_char, "aryta"//c_null_char, "hggth"//c_null_char]

            cp(1) = c_loc(sv(1))
            cp(2) = c_loc(sv(2))
            cp(3) = c_loc(sv(3))
            cp(4) = c_loc(sv(4))

        ! call h5_write('sv.h5:/sv', sv, flags=h5f_acc_trunc)
        ! call h5_write('sv.h5:/sv', sv, dtype=find_h5dtype(sv), flags=h5f_acc_trunc)
        ! call h5_write('sv.h5:/sv', cp, dtype=find_h5dtype(sv), mdims=[4_8], fdims=[4_8], flags=h5f_acc_trunc)
!        call h5_write('sv.h5:/sv', cp, dtype=find_h5dtype(sv), flags=h5f_acc_trunc)
        call h5_write('sv.h5:/sv', cp, dtype=h5t_utf8_v, flags=h5f_acc_trunc)

        end block

        sv = ["yhart", "ffsfr", "aryta", "hggth"]
        call h5_write('sv.h5:/svf', sv, dtype=find_h5dtype(sv), flags=h5f_acc_rdwr)


    end if

#ifdef USE_MPI
    call f % init('fm.h5', flags=h5f_acc_trunc, comm=comm)
    call d % init(f, '/ds2', dtype=find_h5dtype(v), fdims=[4,5]*1_8, mdims=[4,5]*1_8)

!     independent io, processes need to open file and dset collectively but then need not wait for each other
    if (ip < 5) call d % write(v, fidx=[0, ip]*1_8, fcnt=[4,1]*1_8, midx=[0,ip]*1_8, mcnt=[4,1]*1_8, mpio_xfer = h5fd_mpio_independent)

    call d % close()
    call f % close()

    if (np < 5) then
! with separately configured fspace, also using default collective mpi-io
        v_ref(:,:) = v(:,:)

        call s % init(shape(v, 8))
        call s % select_hyperslab(start=[ip,0]*1_8, count=[1,5]*1_8)

        call h5_write('fm.h5:/mpi/v', v, dtype=find_h5dtype(v), fspace=s, comm=comm)

        v = 0

        call h5_read('fm.h5:/mpi/v', v, dtype=find_h5dtype(v), fspace=s, comm=comm)

        do i = 0, np-1
            call mpi_barrier(comm, err)
            if (ip == i) print *, ip
            if (ip == i) print '(4(1x,g0))', v
        end do

        diff = sum(abs(v(ip+1,:) - v_ref(ip+1,:)))
        print *, 'diff', ip, diff
        assert(diff < 1e-12_p)

    end if
#endif




!   will be nice to have some file introspection exposed

    end subroutine s_test


    program test
#ifdef USE_MPI
    use mpi
#endif
    implicit none
#ifdef USE_MPI
    integer :: err
    call mpi_init(err)
#endif
    call s_test()
#ifdef USE_MPI
    call mpi_finalize(err)
#endif
    end program test
