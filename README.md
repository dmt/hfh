# A Handy Fortran 2018 HDF5 wrapper (HFH)

There is already https://github.com/scivision/oo_hdf5_fortran, which is pretty decent.
My project takes a more explicit approach on the idea and was an excersise in learning HDF5.

### Features
  * adds strong typing to HDF5's objects, detects many mistakes at compile time rather than runtime
  * depends only on HDF5's C API avoiding compatibility issues with fortran modules produced by different compilers
  * any array dimension supported with short code and no repetition thanks to the utilisation of modern fortran features
  * parallel IO through MPI-IO exposed

### Shortcoming
  * only fixed size numeric types supported so far
  * depends on very recent fortran features supported only in recent compiler versions.
  * non-standard, very long source lines support needed for the preprocessor macros.
  * automatic type determination could be improved when "class(\*), dimension(\*)" is supported by compilers
  * a small C file needs to be compiled as well

### Prerequisites
  * ninja v1.8+
  * Fortran compiler with draft 2018 support, gfortran v4.9+ and ifort v16+ tested
  * C compiler

### How to use
See build.ninja and test.F90
